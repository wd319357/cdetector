package clonemerge;

import java.util.HashSet;
import java.util.Set;

import org.antlr.v4.runtime.tree.ParseTree;

public class CloneMerger {
	Set<ClonePair> rawClones;
	public CloneMerger(Set<ClonePair> clones) {
		rawClones = clones;
	}
	
	public Set<ClonePair> mergeClones() {
		Set<ClonePair> filteredClones = new HashSet<ClonePair>();
		
		HashSet<ClonePair> subclone =  new HashSet<ClonePair>();
		
		for (ClonePair a : rawClones) {
			for (ClonePair b : rawClones) {
					if (a == b) {
						if (isSelfclone(a)) {
							subclone.add(a);
							break;
						}
					} else if (isSubclone(a, b)) {
						subclone.add(a);
						break;
					}
			}
		}
		
		for (ClonePair a : rawClones) {
			if (!subclone.contains(a)) {
				filteredClones.add(a);
			}
		}
		
		return filteredClones;
	}
	
	
	private boolean isSubclone(ClonePair a, ClonePair b) {
		if (isSubnode(a.getFirst().getParseTreeNode(), b.getFirst().getParseTreeNode()) &&
				isSubnode(a.getSecond().getParseTreeNode(), b.getSecond().getParseTreeNode()))
				return true;
		if (isSubnode(a.getFirst().getParseTreeNode(), b.getSecond().getParseTreeNode()) &&
				isSubnode(a.getSecond().getParseTreeNode(), b.getFirst().getParseTreeNode()))
				return true;
		
		return false;
	}
	
	private boolean isSelfclone(ClonePair a) {
		if (isSubnode(a.getFirst().getParseTreeNode(), a.getSecond().getParseTreeNode()))
			return true;
		if (isSubnode(a.getSecond().getParseTreeNode(), a.getFirst().getParseTreeNode()))
			return true;
		
		return false;
	}
	
	private boolean isSubnode(ParseTree a, ParseTree b) {
		if (a == b) {
			return true;
		}
		
		ParseTree current = a;
		while (current.getParent() != null) {
			current = current.getParent();
			if (current.hashCode() == b.hashCode())
				return true;
		}
		return false;
	}
}
