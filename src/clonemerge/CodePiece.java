package clonemerge;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTree;

public class CodePiece {
	ParseTree parseTreeNode;
	String file;

	public CodePiece (ParseTree parseTreeNode, String file) {
		this.file = file;
		this.parseTreeNode = parseTreeNode;
	}

	public ParseTree getParseTreeNode() {
		return parseTreeNode;
	}

	public void setParseTreeNode(ParseTree parseTreeNode) {
		this.parseTreeNode = parseTreeNode;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}
	
	public int getStartLine() {
		ParserRuleContext pcr = (ParserRuleContext) parseTreeNode;
		return pcr.start.getLine();
	}
	
	public int getStopLine() {
		ParserRuleContext pcr = (ParserRuleContext) parseTreeNode;
		return pcr.stop.getLine();
	}
	
}
