package graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.SingleGraph;
import clonemerge.ClonePair;
import clonemerge.CodePiece;

public class GraphDrawer {
	boolean showLabels;
	Graph graph = new SingleGraph("CloneGraph");

	Set<CodePiece> nodes = new HashSet<CodePiece>();
	Set<ClonePair> edges;
	HashMap<Integer, CodePiece> idToCode = new HashMap<Integer, CodePiece>();
	HashMap<CodePiece, Integer> codeToId = new HashMap<CodePiece, Integer>();
	
	public GraphDrawer (Set<ClonePair> clones, boolean showLabels) {
		edges = clones;
		
		for(ClonePair cp : clones) {
			if (!nodes.contains(cp.getFirst()))
				nodes.add(cp.getFirst());
			if (!nodes.contains(cp.getSecond()))
				nodes.add(cp.getSecond());
		}
		
		this.showLabels = showLabels;
		setgraphStreamStuff();
	}
	
	public void draw() {
		int n = 0;
		for (CodePiece cp : nodes) {
			idToCode.put(n, cp);
			codeToId.put(cp, n);
			n++;
		}
		
		for (int i = 0; i < n; i++) {
			String node = String.valueOf(i);
			String label =  idToCode.get(i).getFile() + ": " + idToCode.get(i).getStartLine() + "-" +  idToCode.get(i).getStopLine();
			graph.addNode(node);
			if (showLabels) {
				graph.getNode(node).setAttribute("ui.label", label);
				graph.getNode(node).setAttribute("layout.weight", 10000);
			}
		}

		for (ClonePair cp : edges) {
			String source = String.valueOf(codeToId.get(cp.getFirst()));
			String destination = String.valueOf(codeToId.get(cp.getSecond()));
			String edgeName = source + "-" + destination;
			
			graph.addEdge(edgeName, source, destination);
			if (showLabels) {
				graph.getEdge(edgeName).setAttribute("ui.label", String.valueOf(Math.round(cp.getSimilarity()*100)/100.0));
				graph.getEdge(edgeName).setAttribute("layout.weight", 50);
			}
		}
		
		graph.display();
		
	}
	
	private void setgraphStreamStuff() {
		System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
		
		//graph = new SingleGraph("CloneGraph");
		graph.addAttribute("ui.antialias");
		
		graph.addAttribute("ui.stylesheet", 
				"graph { padding: 40px; } "
				+ "node { "
				+ "text-mode: normal;"
				//+ "text-visibility-mode: under-zoom;"
				+ "text-visibility: 0.25;"
				+ "text-size: 9;"
				+ "size: 10px; "
				+ "text-alignment: at-right; "
				+ "text-padding: 5px, 5px; "
				+ "text-background-mode: rounded-box; "
				+ "text-background-color: #e8e399; "
				+ "}"
				+ "edge {"
				+ "text-mode: normal;"
				+ "text-visibility-mode: under-zoom;"
				+ "text-visibility: 0.25;"
				+ "text-background-mode: rounded-box; "
				+ "text-background-color: #92c19f; "
				+ "text-padding: 5px, 5px; "
				+ "text-alignment: along; "
				+ "}");
	}
}
