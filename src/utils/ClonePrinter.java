package utils;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import org.antlr.v4.runtime.ParserRuleContext;

import clonemerge.ClonePair;

public class ClonePrinter {
	public static String catLines(String file, int start, int end) throws IOException {
		FileInputStream fstream = new FileInputStream(file);
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

		String line;
		String result="";
		int n = 0;
		while ((line = br.readLine()) != null)   {
			n++;
			if (n > end) 
				break;
			
			if (n >= start) {
				result += line + "\n";
			}
			
		}
		
		//Close the input stream
		br.close();
		return result;
	}
	
	public static void printClonePair(ClonePair cl, String repoPath) throws IOException {
		ParserRuleContext pcrFirst = (ParserRuleContext) cl.getFirst().getParseTreeNode();
		ParserRuleContext pcrSecond = (ParserRuleContext) cl.getSecond().getParseTreeNode();
		
		String filePath = repoPath + "/" + cl.getFirst().getFile();
		int start = pcrFirst.start.getLine();
		int stop = pcrFirst.stop.getLine();

		//System.out.println(cl.getSecond().getFile());
		System.out.println(stop - start + 1);
		System.out.println(catLines(filePath, start, stop));
		

		filePath = repoPath + "/" + cl.getSecond().getFile();
		start = pcrSecond.start.getLine();
		stop = pcrSecond.stop.getLine();
		
		//System.out.println(cl.getFirst().getFile());
		System.out.println(stop - start + 1);
		System.out.println(catLines(filePath, start, stop));
	}
}
