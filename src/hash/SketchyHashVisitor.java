package hash;

import java.util.Map;
import java.util.Vector;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.RuleNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import parser.Java8Parser;

public class SketchyHashVisitor extends SimpleHashVisitor {
	int minimumChildDepth;

	public SketchyHashVisitor(int minimumChildDepth) {
		this.minimumChildDepth = minimumChildDepth; 
	}
	
	@Override
	public Map<ParseTree, Long> visitUnannType(Java8Parser.UnannTypeContext ctx) {
		return defaultResult(ctx, HashTypes.nodeUnification("type"));
	}

	@Override
	public Map<ParseTree, Long> visitExpressionName(Java8Parser.ExpressionNameContext ctx) {
		return defaultResult(ctx, HashTypes.nodeUnification("variable"));
	}
	

	@Override
	public Map<ParseTree, Long> visitChildren(RuleNode node) {
		Map<ParseTree, Long> result = defaultResult();
		int n = node.getChildCount();
		Vector<Long> hashes= new Vector<Long>();
		
		for (int i = 0; i < n; i++) {
			if (!shouldVisitNextChild(node, result)) {
				break;
			}
			
			ParseTree c = node.getChild(i);
			Map<ParseTree, Long> childResult = c.accept(this);
			
			// if child is of desired depth
			// NOTE: can optimize
			if (new DepthVisitor().visit(c) > minimumChildDepth) {
				hashes.add(childResult.get(c));
			}

			if (c instanceof TerminalNode) {
				childResult.remove(c);
			}
			result = aggregateResult(result, childResult);
		}
		
		hashes.add(new Long(node.getRuleContext().getRuleIndex()));
		
		if (n == 1) {
			result.remove(node.getChild(0));
			result.put(node, new Long(node.getRuleContext().getRuleIndex()));
			return result;
		}
		
		Long hash = hashFunction(hashes);
		result.put(node, hash);
		
		return result;
	}
}
