// Generated from Java8.g4 by ANTLR 4.5
package parser;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.AbstractParseTreeVisitor;

/**
 * This class provides an empty implementation of {@link Java8Visitor},
 * which can be extended to create a visitor which only needs to handle a subset
 * of the available methods.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public class Java8BaseVisitor<T> extends AbstractParseTreeVisitor<T> implements Java8Visitor<T> {
	
	public void everyVisit(ParserRuleContext ctx, T result) { };
	
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitLiteral(Java8Parser.LiteralContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitType(Java8Parser.TypeContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPrimitiveType(Java8Parser.PrimitiveTypeContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitNumericType(Java8Parser.NumericTypeContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitIntegralType(Java8Parser.IntegralTypeContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitFloatingPointType(Java8Parser.FloatingPointTypeContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitReferenceType(Java8Parser.ReferenceTypeContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitClassOrInterfaceType(Java8Parser.ClassOrInterfaceTypeContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitClassType(Java8Parser.ClassTypeContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitClassType_lf_classOrInterfaceType(Java8Parser.ClassType_lf_classOrInterfaceTypeContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitClassType_lfno_classOrInterfaceType(Java8Parser.ClassType_lfno_classOrInterfaceTypeContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitInterfaceType(Java8Parser.InterfaceTypeContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitInterfaceType_lf_classOrInterfaceType(Java8Parser.InterfaceType_lf_classOrInterfaceTypeContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitInterfaceType_lfno_classOrInterfaceType(Java8Parser.InterfaceType_lfno_classOrInterfaceTypeContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitTypeVariable(Java8Parser.TypeVariableContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitArrayType(Java8Parser.ArrayTypeContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitDims(Java8Parser.DimsContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitTypeParameter(Java8Parser.TypeParameterContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitTypeParameterModifier(Java8Parser.TypeParameterModifierContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitTypeBound(Java8Parser.TypeBoundContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitAdditionalBound(Java8Parser.AdditionalBoundContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitTypeArguments(Java8Parser.TypeArgumentsContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitTypeArgumentList(Java8Parser.TypeArgumentListContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitTypeArgument(Java8Parser.TypeArgumentContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitWildcard(Java8Parser.WildcardContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitWildcardBounds(Java8Parser.WildcardBoundsContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPackageName(Java8Parser.PackageNameContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitTypeName(Java8Parser.TypeNameContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPackageOrTypeName(Java8Parser.PackageOrTypeNameContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitExpressionName(Java8Parser.ExpressionNameContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitMethodName(Java8Parser.MethodNameContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitAmbiguousName(Java8Parser.AmbiguousNameContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitCompilationUnit(Java8Parser.CompilationUnitContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPackageDeclaration(Java8Parser.PackageDeclarationContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPackageModifier(Java8Parser.PackageModifierContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitImportDeclaration(Java8Parser.ImportDeclarationContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitSingleTypeImportDeclaration(Java8Parser.SingleTypeImportDeclarationContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitTypeImportOnDemandDeclaration(Java8Parser.TypeImportOnDemandDeclarationContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitSingleStaticImportDeclaration(Java8Parser.SingleStaticImportDeclarationContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitStaticImportOnDemandDeclaration(Java8Parser.StaticImportOnDemandDeclarationContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitTypeDeclaration(Java8Parser.TypeDeclarationContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitClassDeclaration(Java8Parser.ClassDeclarationContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitNormalClassDeclaration(Java8Parser.NormalClassDeclarationContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitClassModifier(Java8Parser.ClassModifierContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitTypeParameters(Java8Parser.TypeParametersContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitTypeParameterList(Java8Parser.TypeParameterListContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitSuperclass(Java8Parser.SuperclassContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitSuperinterfaces(Java8Parser.SuperinterfacesContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitInterfaceTypeList(Java8Parser.InterfaceTypeListContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitClassBody(Java8Parser.ClassBodyContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitClassBodyDeclaration(Java8Parser.ClassBodyDeclarationContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitClassMemberDeclaration(Java8Parser.ClassMemberDeclarationContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitFieldDeclaration(Java8Parser.FieldDeclarationContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitFieldModifier(Java8Parser.FieldModifierContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitVariableDeclaratorList(Java8Parser.VariableDeclaratorListContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitVariableDeclarator(Java8Parser.VariableDeclaratorContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitVariableDeclaratorId(Java8Parser.VariableDeclaratorIdContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitVariableInitializer(Java8Parser.VariableInitializerContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitUnannType(Java8Parser.UnannTypeContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitUnannPrimitiveType(Java8Parser.UnannPrimitiveTypeContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitUnannReferenceType(Java8Parser.UnannReferenceTypeContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitUnannClassOrInterfaceType(Java8Parser.UnannClassOrInterfaceTypeContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitUnannClassType(Java8Parser.UnannClassTypeContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitUnannClassType_lf_unannClassOrInterfaceType(Java8Parser.UnannClassType_lf_unannClassOrInterfaceTypeContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitUnannClassType_lfno_unannClassOrInterfaceType(Java8Parser.UnannClassType_lfno_unannClassOrInterfaceTypeContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitUnannInterfaceType(Java8Parser.UnannInterfaceTypeContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitUnannInterfaceType_lf_unannClassOrInterfaceType(Java8Parser.UnannInterfaceType_lf_unannClassOrInterfaceTypeContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitUnannInterfaceType_lfno_unannClassOrInterfaceType(Java8Parser.UnannInterfaceType_lfno_unannClassOrInterfaceTypeContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitUnannTypeVariable(Java8Parser.UnannTypeVariableContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitUnannArrayType(Java8Parser.UnannArrayTypeContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitMethodDeclaration(Java8Parser.MethodDeclarationContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitMethodModifier(Java8Parser.MethodModifierContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitMethodHeader(Java8Parser.MethodHeaderContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitResult(Java8Parser.ResultContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitMethodDeclarator(Java8Parser.MethodDeclaratorContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitFormalParameterList(Java8Parser.FormalParameterListContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitFormalParameters(Java8Parser.FormalParametersContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitFormalParameter(Java8Parser.FormalParameterContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitVariableModifier(Java8Parser.VariableModifierContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitLastFormalParameter(Java8Parser.LastFormalParameterContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitReceiverParameter(Java8Parser.ReceiverParameterContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitThrows_(Java8Parser.Throws_Context ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitExceptionTypeList(Java8Parser.ExceptionTypeListContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitExceptionType(Java8Parser.ExceptionTypeContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitMethodBody(Java8Parser.MethodBodyContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitInstanceInitializer(Java8Parser.InstanceInitializerContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitStaticInitializer(Java8Parser.StaticInitializerContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitConstructorDeclaration(Java8Parser.ConstructorDeclarationContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitConstructorModifier(Java8Parser.ConstructorModifierContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitConstructorDeclarator(Java8Parser.ConstructorDeclaratorContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitSimpleTypeName(Java8Parser.SimpleTypeNameContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitConstructorBody(Java8Parser.ConstructorBodyContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitExplicitConstructorInvocation(Java8Parser.ExplicitConstructorInvocationContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitEnumDeclaration(Java8Parser.EnumDeclarationContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitEnumBody(Java8Parser.EnumBodyContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitEnumConstantList(Java8Parser.EnumConstantListContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitEnumConstant(Java8Parser.EnumConstantContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitEnumConstantModifier(Java8Parser.EnumConstantModifierContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitEnumBodyDeclarations(Java8Parser.EnumBodyDeclarationsContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitInterfaceDeclaration(Java8Parser.InterfaceDeclarationContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitNormalInterfaceDeclaration(Java8Parser.NormalInterfaceDeclarationContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitInterfaceModifier(Java8Parser.InterfaceModifierContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitExtendsInterfaces(Java8Parser.ExtendsInterfacesContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitInterfaceBody(Java8Parser.InterfaceBodyContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitInterfaceMemberDeclaration(Java8Parser.InterfaceMemberDeclarationContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitConstantDeclaration(Java8Parser.ConstantDeclarationContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitConstantModifier(Java8Parser.ConstantModifierContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitInterfaceMethodDeclaration(Java8Parser.InterfaceMethodDeclarationContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitInterfaceMethodModifier(Java8Parser.InterfaceMethodModifierContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitAnnotationTypeDeclaration(Java8Parser.AnnotationTypeDeclarationContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitAnnotationTypeBody(Java8Parser.AnnotationTypeBodyContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitAnnotationTypeMemberDeclaration(Java8Parser.AnnotationTypeMemberDeclarationContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitAnnotationTypeElementDeclaration(Java8Parser.AnnotationTypeElementDeclarationContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitAnnotationTypeElementModifier(Java8Parser.AnnotationTypeElementModifierContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitDefaultValue(Java8Parser.DefaultValueContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitAnnotation(Java8Parser.AnnotationContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitNormalAnnotation(Java8Parser.NormalAnnotationContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitElementValuePairList(Java8Parser.ElementValuePairListContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitElementValuePair(Java8Parser.ElementValuePairContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitElementValue(Java8Parser.ElementValueContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitElementValueArrayInitializer(Java8Parser.ElementValueArrayInitializerContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitElementValueList(Java8Parser.ElementValueListContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitMarkerAnnotation(Java8Parser.MarkerAnnotationContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitSingleElementAnnotation(Java8Parser.SingleElementAnnotationContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitArrayInitializer(Java8Parser.ArrayInitializerContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitVariableInitializerList(Java8Parser.VariableInitializerListContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitBlock(Java8Parser.BlockContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitBlockStatements(Java8Parser.BlockStatementsContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitBlockStatement(Java8Parser.BlockStatementContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitLocalVariableDeclarationStatement(Java8Parser.LocalVariableDeclarationStatementContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitLocalVariableDeclaration(Java8Parser.LocalVariableDeclarationContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitStatement(Java8Parser.StatementContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitStatementNoShortIf(Java8Parser.StatementNoShortIfContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitStatementWithoutTrailingSubstatement(Java8Parser.StatementWithoutTrailingSubstatementContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitEmptyStatement(Java8Parser.EmptyStatementContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitLabeledStatement(Java8Parser.LabeledStatementContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitLabeledStatementNoShortIf(Java8Parser.LabeledStatementNoShortIfContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitExpressionStatement(Java8Parser.ExpressionStatementContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitStatementExpression(Java8Parser.StatementExpressionContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitIfThenStatement(Java8Parser.IfThenStatementContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitIfThenElseStatement(Java8Parser.IfThenElseStatementContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitIfThenElseStatementNoShortIf(Java8Parser.IfThenElseStatementNoShortIfContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitAssertStatement(Java8Parser.AssertStatementContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitSwitchStatement(Java8Parser.SwitchStatementContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitSwitchBlock(Java8Parser.SwitchBlockContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitSwitchBlockStatementGroup(Java8Parser.SwitchBlockStatementGroupContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitSwitchLabels(Java8Parser.SwitchLabelsContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitSwitchLabel(Java8Parser.SwitchLabelContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitEnumConstantName(Java8Parser.EnumConstantNameContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitWhileStatement(Java8Parser.WhileStatementContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitWhileStatementNoShortIf(Java8Parser.WhileStatementNoShortIfContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitDoStatement(Java8Parser.DoStatementContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitForStatement(Java8Parser.ForStatementContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitForStatementNoShortIf(Java8Parser.ForStatementNoShortIfContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitBasicForStatement(Java8Parser.BasicForStatementContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitBasicForStatementNoShortIf(Java8Parser.BasicForStatementNoShortIfContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitForInit(Java8Parser.ForInitContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitForUpdate(Java8Parser.ForUpdateContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitStatementExpressionList(Java8Parser.StatementExpressionListContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitEnhancedForStatement(Java8Parser.EnhancedForStatementContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitEnhancedForStatementNoShortIf(Java8Parser.EnhancedForStatementNoShortIfContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitBreakStatement(Java8Parser.BreakStatementContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitContinueStatement(Java8Parser.ContinueStatementContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitReturnStatement(Java8Parser.ReturnStatementContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitThrowStatement(Java8Parser.ThrowStatementContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitSynchronizedStatement(Java8Parser.SynchronizedStatementContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitTryStatement(Java8Parser.TryStatementContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitCatches(Java8Parser.CatchesContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitCatchClause(Java8Parser.CatchClauseContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitCatchFormalParameter(Java8Parser.CatchFormalParameterContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitCatchType(Java8Parser.CatchTypeContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitFinally_(Java8Parser.Finally_Context ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitTryWithResourcesStatement(Java8Parser.TryWithResourcesStatementContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitResourceSpecification(Java8Parser.ResourceSpecificationContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitResourceList(Java8Parser.ResourceListContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitResource(Java8Parser.ResourceContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPrimary(Java8Parser.PrimaryContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPrimaryNoNewArray(Java8Parser.PrimaryNoNewArrayContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPrimaryNoNewArray_lf_arrayAccess(Java8Parser.PrimaryNoNewArray_lf_arrayAccessContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPrimaryNoNewArray_lfno_arrayAccess(Java8Parser.PrimaryNoNewArray_lfno_arrayAccessContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPrimaryNoNewArray_lf_primary(Java8Parser.PrimaryNoNewArray_lf_primaryContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPrimaryNoNewArray_lf_primary_lf_arrayAccess_lf_primary(Java8Parser.PrimaryNoNewArray_lf_primary_lf_arrayAccess_lf_primaryContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPrimaryNoNewArray_lf_primary_lfno_arrayAccess_lf_primary(Java8Parser.PrimaryNoNewArray_lf_primary_lfno_arrayAccess_lf_primaryContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPrimaryNoNewArray_lfno_primary(Java8Parser.PrimaryNoNewArray_lfno_primaryContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPrimaryNoNewArray_lfno_primary_lf_arrayAccess_lfno_primary(Java8Parser.PrimaryNoNewArray_lfno_primary_lf_arrayAccess_lfno_primaryContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPrimaryNoNewArray_lfno_primary_lfno_arrayAccess_lfno_primary(Java8Parser.PrimaryNoNewArray_lfno_primary_lfno_arrayAccess_lfno_primaryContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitClassInstanceCreationExpression(Java8Parser.ClassInstanceCreationExpressionContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitClassInstanceCreationExpression_lf_primary(Java8Parser.ClassInstanceCreationExpression_lf_primaryContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitClassInstanceCreationExpression_lfno_primary(Java8Parser.ClassInstanceCreationExpression_lfno_primaryContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitTypeArgumentsOrDiamond(Java8Parser.TypeArgumentsOrDiamondContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitFieldAccess(Java8Parser.FieldAccessContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitFieldAccess_lf_primary(Java8Parser.FieldAccess_lf_primaryContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitFieldAccess_lfno_primary(Java8Parser.FieldAccess_lfno_primaryContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitArrayAccess(Java8Parser.ArrayAccessContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitArrayAccess_lf_primary(Java8Parser.ArrayAccess_lf_primaryContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitArrayAccess_lfno_primary(Java8Parser.ArrayAccess_lfno_primaryContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitMethodInvocation(Java8Parser.MethodInvocationContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitMethodInvocation_lf_primary(Java8Parser.MethodInvocation_lf_primaryContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitMethodInvocation_lfno_primary(Java8Parser.MethodInvocation_lfno_primaryContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitArgumentList(Java8Parser.ArgumentListContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitMethodReference(Java8Parser.MethodReferenceContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitMethodReference_lf_primary(Java8Parser.MethodReference_lf_primaryContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitMethodReference_lfno_primary(Java8Parser.MethodReference_lfno_primaryContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitArrayCreationExpression(Java8Parser.ArrayCreationExpressionContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitDimExprs(Java8Parser.DimExprsContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitDimExpr(Java8Parser.DimExprContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitConstantExpression(Java8Parser.ConstantExpressionContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitExpression(Java8Parser.ExpressionContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitLambdaExpression(Java8Parser.LambdaExpressionContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitLambdaParameters(Java8Parser.LambdaParametersContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitInferredFormalParameterList(Java8Parser.InferredFormalParameterListContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitLambdaBody(Java8Parser.LambdaBodyContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitAssignmentExpression(Java8Parser.AssignmentExpressionContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitAssignment(Java8Parser.AssignmentContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitLeftHandSide(Java8Parser.LeftHandSideContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitAssignmentOperator(Java8Parser.AssignmentOperatorContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitConditionalExpression(Java8Parser.ConditionalExpressionContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitConditionalOrExpression(Java8Parser.ConditionalOrExpressionContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitConditionalAndExpression(Java8Parser.ConditionalAndExpressionContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitInclusiveOrExpression(Java8Parser.InclusiveOrExpressionContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitExclusiveOrExpression(Java8Parser.ExclusiveOrExpressionContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitAndExpression(Java8Parser.AndExpressionContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitEqualityExpression(Java8Parser.EqualityExpressionContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitRelationalExpression(Java8Parser.RelationalExpressionContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitShiftExpression(Java8Parser.ShiftExpressionContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitAdditiveExpression(Java8Parser.AdditiveExpressionContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitMultiplicativeExpression(Java8Parser.MultiplicativeExpressionContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitUnaryExpression(Java8Parser.UnaryExpressionContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPreIncrementExpression(Java8Parser.PreIncrementExpressionContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPreDecrementExpression(Java8Parser.PreDecrementExpressionContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitUnaryExpressionNotPlusMinus(Java8Parser.UnaryExpressionNotPlusMinusContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPostfixExpression(Java8Parser.PostfixExpressionContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPostIncrementExpression(Java8Parser.PostIncrementExpressionContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPostIncrementExpression_lf_postfixExpression(Java8Parser.PostIncrementExpression_lf_postfixExpressionContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPostDecrementExpression(Java8Parser.PostDecrementExpressionContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitPostDecrementExpression_lf_postfixExpression(Java8Parser.PostDecrementExpression_lf_postfixExpressionContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.</p>
	 */
	@Override public T visitCastExpression(Java8Parser.CastExpressionContext ctx) { 
		T result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result;
	}
}