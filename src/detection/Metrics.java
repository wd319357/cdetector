package detection;

public interface Metrics<T> {
	double distance(T a, T b);
}