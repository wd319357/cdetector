package detection;

import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTree;

import clonemerge.*;

public class CloneDetector {
	int minimumCloneSize;
	double minimalSimilarity;

	ComponentSetMetrics metrics;

	Map<ParseTree, String> functionToFile = new IdentityHashMap<ParseTree, String>();
	Map<Long, Set<ParserRuleContext>> oldFunctions = new HashMap<Long, Set<ParserRuleContext>>();
	Map<Long, Set<ParserRuleContext>> modifiedFunctions = new HashMap<Long, Set<ParserRuleContext>>();
	Set<ClonePair> rawClones = new HashSet<ClonePair>();
	
	
	public CloneDetector(double minimalSimilarity, int minimumCloneSize, ComponentSetMetrics metrics) {
		this.minimalSimilarity = minimalSimilarity;
		this.minimumCloneSize = minimumCloneSize;
		this.metrics = metrics;
	}
			
	public void feed(
		Map<ParseTree, String> ftf,
		Map<ParseTree, Long> of,
		Map<ParseTree, Long> mf) {
		
		functionToFile = ftf;
		
		for (ParseTree pt: of.keySet()) {
			ParserRuleContext pcr = (ParserRuleContext) pt;
			if (!isWorthy(pcr))
				continue;
			Long hash = of.get(pt);
			
			if (!oldFunctions.containsKey(hash)) {
				oldFunctions.put(hash, new HashSet<ParserRuleContext>());
			}
			oldFunctions.get(hash).add((ParserRuleContext)pt);
		}
		
		
		for (ParseTree pt: mf.keySet()) {
			ParserRuleContext pcr = (ParserRuleContext) pt;
			if (!isWorthy(pcr))
				continue;
			Long hash = mf.get(pt);
			
			if (!modifiedFunctions.containsKey(hash)) {
				modifiedFunctions.put(hash, new HashSet<ParserRuleContext>());
			}
			modifiedFunctions.get(hash).add((ParserRuleContext)pt);
		}
	}
	
	public Set<ClonePair> detect() {
		HashMap<ParseTree, CodePiece> codePieces = new HashMap<ParseTree, CodePiece>();
		
		for (Long hash: modifiedFunctions.keySet()) {
			Set<ParserRuleContext> treeSet = modifiedFunctions.get(hash);
			if (treeSet.size() <= 1)
				continue;
			
			for (ParseTree first: treeSet) {
				boolean ignoring = true;
				for (ParseTree second: treeSet) {
					if (ignoring) {
						if (first == second ) {
							ignoring = false;
						}
						continue;
					}
					
					double similarity = metrics.distance(first, second);

					if (similarity > minimalSimilarity) {
						CodePiece firstCodePiece;
						CodePiece secondCodePiece;
						
						if (codePieces.containsKey(first)) {
							firstCodePiece = codePieces.get(first);
						}
						else {
							firstCodePiece = new CodePiece(first, functionToFile.get(first));
							codePieces.put(first, firstCodePiece);
						}
						
						if (codePieces.containsKey(second)) {
							secondCodePiece = codePieces.get(second);
						}
						else {
							secondCodePiece = new CodePiece(second, functionToFile.get(second));
							codePieces.put(second, secondCodePiece);
						}

						rawClones.add(new ClonePair(firstCodePiece, secondCodePiece, similarity));
					}
				}
			}
		}
		
		if (modifiedFunctions != oldFunctions) {			
			for (Long hash: modifiedFunctions.keySet()) {
				Set<ParserRuleContext> treeSetMod = modifiedFunctions.get(hash);
				Set<ParserRuleContext> treeSetOld = oldFunctions.get(hash);
				
				if (treeSetOld == null)
					continue;
				
				if (treeSetMod.size() + treeSetOld.size() <= 1)
					continue;
				
				for (ParseTree first: treeSetMod) {
					for (ParseTree second: treeSetOld) {
						if (functionToFile.get(first).equals(functionToFile.get(second))) {
							continue;
						}
						
						double similarity = metrics.distance(first, second);
	
						if (similarity > minimalSimilarity) {
							CodePiece firstCodePiece;
							CodePiece secondCodePiece;
							
							if (codePieces.containsKey(first)) {
								firstCodePiece = codePieces.get(first);
							}
							else {
								firstCodePiece = new CodePiece(first, functionToFile.get(first));
								codePieces.put(first, firstCodePiece);
							}
							
							if (codePieces.containsKey(second)) {
								secondCodePiece = codePieces.get(second);
							}
							else {
								secondCodePiece = new CodePiece(second, functionToFile.get(second));
								codePieces.put(second, secondCodePiece);
							}
	
							rawClones.add(new ClonePair(firstCodePiece, secondCodePiece, similarity));
						}
					}
				}
			}
		}
		
		CloneMerger cm = new CloneMerger(rawClones);
		return cm.mergeClones();
	}
	
	// NOTE: code size measure could be replaced
	protected boolean isWorthy(ParserRuleContext pcr) {
		return pcr.stop.getLine() - pcr.start.getLine() >= minimumCloneSize;
	}
	
}
